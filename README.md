<p align="center"><img src="https://codeberg.org/joaoh/xploit-framework/raw/branch/main/logo/logo.png"/></p>

<i><p align="center">The Recon Framework (TRF) is a recon framework coded in the GoLang programming language.</p></i>

<b>Installation:</b>
```
git clone https://codeberg.org/joaoh/xploit-framework && cd xploit-framework && go build -ldflags="-s -w"
```

<b>Run:</b>
```
./xploit
```

<p>Tools:</p>
<blockquote>
<li>DNS RECON</li>
<li>WHOIS</li>
<li>DIR SEARCH</li>
<li>ROBOTS FINDER</li>
<li>PORT SCANNER</li>
<li>SUBDOMAIN SCANNER</li>
<li>BANNER GRAB</li>
<li>GEOIP</li>
</blockquote>

<i>Re-upload from https://codeberg.org/joaoh/xploit-framework</i>
